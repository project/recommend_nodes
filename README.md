# Recommend nodes

This module gives the functionality of recommendation of nodes.

If the user wants to recommend that node to some other users of the website by
selecting the user name and recommend note. This module provides a link
(Recommend) on the node detail page for those node types whose setting for
(Recommendation Node Type) is enabled.

- For a full description of the module, visit the
  [project page](https://drupal.org/project/recommend_nodes).

- To submit bug reports and feature suggestions, or to track changes
  [issue queue](https://drupal.org/project/issues/recommend_nodes).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. Visit:
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules)
for further information.


## Configuration

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > System > Recommend nodes to
   configure.
3. Select the content type(s) for which the node recommendation should be
   enabled.
4. Save Configuration.


## Maintainers

- [Himmat Bhatia (himmatbhatia)](https://www.drupal.org/u/himmatbhatia)

**This project is sponsored by:**

- [Reliance Jio Infocomm Limited](https://www.drupal.orgreliance-jio-infocomm-limited)
