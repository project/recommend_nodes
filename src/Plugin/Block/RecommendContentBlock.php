<?php

namespace Drupal\recommend_nodes\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'RecommendContentBlock' block.
 *
 * @Block(
 *  id = "recommend_content_block",
 *  admin_label = @Translation("Recommend content block"),
 * )
 */
class RecommendContentBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->database = $container->get('database');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $header = [
        ['data' => t('Title'), 'field' => 'title'],
        ['data' => t('Recommend Note'), 'field' => 'recommend_text'],
        ['data' => t('Recommend By'), 'field' => 'recommend_by'],
    ];
    $query = $this->database->select('recommend_nodes', 'nr');
    $query->leftjoin('node_field_data', 'n', 'n.nid = nr.nid');
    $query->fields('n', ['title', 'nid']);
    $query->fields('nr', ['recommend_text', 'recommend_by']);
    $query->condition('nr.recommend_to', $this->currentUser->id());
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $data = $pager->execute()->fetchAll();
    foreach ($data as $row) {
      $user = \Drupal::entityTypeManager()->getStorage('user')->load($row->recommend_by);
      $name = $user->get('name')->value;
      $row->recommend_by = $name;
      $url = Url::fromRoute('entity.node.canonical', ['node' => $row->nid]);
      $title = Link::fromTextAndUrl(t($row->title), $url)->toString();
      $row->title = $title;
      unset($row->nid);
      $rows[] = ['data' => (array) $row];
    }
    $build['location_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
