<?php

namespace Drupal\recommend_nodes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Class RecommendForm.
 */
class RecommendForm extends FormBase {
  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountProxyInterface $currentUserManager, Connection $dbconnection, MessengerInterface $messenger) {
    $this->currentUser = $currentUserManager;
    $this->connection = $dbconnection;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
          $container->get('current_user'),
          $container->get('database'),
    $container->get('messenger')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recommend_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $form['user_name'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#tags' => TRUE,
      '#required' => TRUE,
      '#title' => $this->t('User name'),
      '#description' => $this->t('Select a user to whom you want to recommend'),
      '#placeholder' => $this->t('User to whom you want to recommend comma seprated for multiple users'),
      '#weight' => '0',
    ];
    $form['recommend_text'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#maxlength' => 128,
      '#title' => $this->t('Recommend note'),
    ];
    $form['id'] = [
      '#type' => 'hidden',
      '#value' => $id,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('user_name') as $value) {
      $query = $this->connection->select('recommend_nodes', 'nr');
      $query->leftjoin('node_field_data', 'n', 'n.nid = nr.nid');
      $query->fields('n', ['nid']);
      $query->condition('nr.recommend_to', $value['target_id']);
      $query->condition('n.nid', $form_state->getValue('id'));
      $query->condition('nr.recommend_by', $this->currentUser->id());
      $result = $query->execute()->fetchAll();
      $user = \Drupal::entityTypeManager()->getStorage('user')->load($value['target_id']);
      $name = $user->get('name')->value;
      if (empty($result)) {
        $this->connection->insert('recommend_nodes')
          ->fields(
                  [
                    'recommend_to' => $value['target_id'],
                    'recommend_by' => $this->currentUser->id(),
                    'recommend_text' => $form_state->getValue('recommend_text'),
                    'nid' => $form_state->getValue('id'),
                    'recommend_date' => \Drupal::time()->getRequestTime(),
                    'accept_deny' => 0,
                  ]
              )
          ->execute();

        $this->messenger->addStatus($this->t('You have successfully recommended the post to @name', ['@name' => $name]));
      }
      else {
        $this->messenger->addWarning($this->t('You have already recommended this post to @name', ['@name' => $name]), 'warning');
      }
    }
    $url = Url::fromRoute('entity.node.canonical', ['node' => $form_state->getValue('id')]);
    return $form_state->setRedirectUrl($url);
  }

}