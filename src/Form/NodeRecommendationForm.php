<?php

namespace Drupal\recommend_nodes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class NodeRecommendationForm.
 */
class NodeRecommendationForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entitymanager) {

    $this->entityTypeManager = $entitymanager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'recommend_nodes.noderecommendation',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recommend_nodes_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('recommend_nodes.noderecommendation');
    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($types as $type) {
      $content_types[$type->id()] = $type->label();
    }
    $form['recommendation_node_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Recommendation Node Type'),
      '#description' => $this->t('Recommendation node type entity'),
      '#options' => $content_types,
      '#default_value' => $config->get('recommendation_node_type'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('recommend_nodes.noderecommendation')
      ->set('recommendation_node_type', $form_state->getValue('recommendation_node_type'))
      ->save();
  }

}
